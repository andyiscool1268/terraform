# Welcome to KodeKloud Terraform Lab :rocket:

This is your Visual Studio Code editor where you will learn and practice about Terraform.

To begin with :tropical_drink: relax and start working on the questions on the left. 

All the best!! :muscle: 

To open a new terminal Press:

MAC:

⌃ + :arrow_up:  + `

Windows:

Ctrl + Shift + `

