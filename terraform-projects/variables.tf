variable "prefix" {
  default = "Mr"
}

variable "separator" {
  default = "."
}

variable "length" {
  default = "2"
}
