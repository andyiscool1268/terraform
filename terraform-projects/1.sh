terraform init 
terraform plan
terraform apply
terraform output pet-name
terraform validate
terraform fmt
terraform show
terraform show -json
terraform providers
terraform providers mirror /root/terraform/new_local_file
terraform refresh
terraform graph
apt update
apt install graphviz -y
terraform graph | dot -Tsvg > graph.svg
terraform console
