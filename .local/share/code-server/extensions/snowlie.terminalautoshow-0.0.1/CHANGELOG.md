# Change Log
All notable changes to the "terminalautoshow" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.0.1] - 2018-01-29
Initial release