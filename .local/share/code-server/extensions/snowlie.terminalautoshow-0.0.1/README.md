# terminalautoshow README

This is a simple extension that will automatically show a terminal when there are no open files and hide it when there is an open file.

## Plans
* Current plans to add configurable settings such as
    - ForceHide: Boolean - Whether to force the terminal to hide if debug, problems, or outut are the currently active tabs.
    - ShowOnStart: Boolean - Show the terminal at the start
    - Enable: Boolean - Enable the extension
* Commands
    - Enable
    - Disable